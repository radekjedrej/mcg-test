<article @php(post_class('team-member row'))>
  <div class="col-md-4">
    <p><a href="{{ get_permalink() }}">{!! the_post_thumbnail('post-featured') !!}</a></p>
  </div>
  <div class="col-md-8 text-md-left">
    <h2 class="entry-title">
      {{ get_the_title() }}
      <small>{{ get_field('member_title') }}</small>
    </h2>
    <div class="entry-summary hidden-sm-down">
      @php(the_excerpt())
    </div>
  </div>
</article>