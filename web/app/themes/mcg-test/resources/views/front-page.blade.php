@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <?php
    var_dump( $header_image ); ?>
    {{ $header_content }}
    @include('partials.content-page')
  @endwhile
@endsection
